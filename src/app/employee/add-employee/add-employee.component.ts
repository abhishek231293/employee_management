import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {EmployeeService} from '../service/employee.service';
import {Employee} from '../model/employee.model';
import {ActivatedRoute, Router, ParamMap} from '@angular/router';
import * as uuid from 'uuid';
import { switchMap } from 'rxjs/operators';
import {isNullOrUndefined} from 'util';
import {Department} from '../model/department.model';
import * as moment from 'moment';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {

  employeeForm: FormGroup;
  submitted: boolean;
  message: string;
  minDate: Date;
  maxDate: Date;
  invalidAge = false;
  employee: Employee;
  departmentList: Department[];
  requestType: string;
  heading: string;
  bsDateConfig = { dateInputFormat: 'MM/DD/YYYY', isAnimated: true, containerClass: 'theme-dark-blue' };

  constructor(
               private employeeService: EmployeeService,
               private activatedroute: ActivatedRoute,
               private router: Router
  ) {
    this.submitted = false;
    this.requestType = 'add';

    this.maxDate = new Date();
    this.minDate = new Date();
    this.maxDate.setFullYear(new Date().getFullYear() - 20);
    this.minDate.setFullYear(new Date().getFullYear() - 60);
  }

  ngOnInit() {
    this.employeeService.getAllDepartment().subscribe((value) => {
      this.departmentList = value;
    });

    this.activatedroute.data.subscribe(data => {
      if (data.request_for) {
        this.requestType = data.request_for;
        this.heading = data.heading;
      }
    });

    if (this.requestType !== 'add') {
      const employee$ = this.activatedroute.paramMap.pipe(
        switchMap((params: ParamMap) =>
          this.employeeService.getEmployee(params.get('id'))
      ));

      employee$.subscribe(employee => {
        if (!isNullOrUndefined(employee)) {
          this.employee = employee;
        } else {
          this.router.navigate(['/employee/add']);
        }
      });
    }

    this.createFormGroup();
  }

  createFormGroup() {
    this.employeeForm = new FormGroup({
      firstName: new FormControl(((this.employee && this.employee.firstName) ? this.employee.firstName : ''), [Validators.required]),
      lastName: new FormControl(((this.employee && this.employee.lastName) ? this.employee.lastName : ''), [Validators.required]),
      dob: new FormControl(((this.employee && this.employee.dob) ? this.employee.dob : ''), [Validators.required]),
      gender: new FormControl(((this.employee && this.employee.gender) ? this.employee.gender : 'male'), [Validators.required]),
      departmentName: new FormControl( ( (this.employee && this.employee.departmentName)
                                          ? this.employee.departmentName : ''), [Validators.required]),
    });

    if (this.requestType === 'view') {
      this.employeeForm.disable();
    }

  }

  addNewEmployee() {

    this.submitted = true;
    this.message = '';

    if (this.employeeForm.valid) {
      const employee = new Employee(
                                   uuid.v4(),
                                   this.employeeForm.value.firstName,
                                   this.employeeForm.value.lastName,
                                   moment(this.employeeForm.value.dob).format('MM/DD/YYYY'),
                                   this.employeeForm.value.gender,
                                   this.employeeForm.value.departmentName,
                                  );
      this.employeeService.addEmployee(employee);
      this.submitted = false;
      this.router.navigate(['/employee/list']);
    } else {
      this.message = 'All fields are required!';
    }
  }

  updateEmployee() {

    this.submitted = true;
    this.message = '';

    if (this.employeeForm.valid) {
      const employee = new Employee(
                                   this.employee.id,
                                   this.employeeForm.value.firstName,
                                   this.employeeForm.value.lastName,
                                   moment(this.employeeForm.value.dob).format('MM/DD/YYYY'),
                                   this.employeeForm.value.gender,
                                   this.employeeForm.value.departmentName,
                                  );
      this.employeeService.updateEmployee(employee);
      this.submitted = false;
      this.router.navigate(['/employee/view/' + this.employee.id]);
    } else {
      this.message = 'All fields are required!';
    }
  }

}
