import { Component, OnInit } from '@angular/core';
import {EmployeeService} from '../service/employee.service';
import {Employee} from '../model/employee.model';

@Component({
  selector: 'app-list-employee',
  templateUrl: './list-employee.component.html',
  styleUrls: ['./list-employee.component.css']
})
export class ListEmployeeComponent implements OnInit {

  public employeeList: Employee[] = null;
  constructor( private employeeService: EmployeeService ) { }

  ngOnInit() {
    this.employeeService.getAllEmployees().subscribe((value) => {
      this.employeeList = value;
    });
    console.log(this.employeeList);
  }

  deleteEmployee(employeeId: string) {
    this.employeeService.deleteEmployee(employeeId).subscribe((value) => {
      this.employeeList = value;
    });
  }

}
