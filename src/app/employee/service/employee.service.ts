import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Employee } from '../model/employee.model';
import { Department } from '../model/department.model';

@Injectable({
  providedIn: 'root',
})

export class EmployeeService {

  private allEmployees: Array<Employee> = [];
  private department: Array<Department> = [
    { id: 1, departmentName: 'Development' },
    { id: 2, departmentName: 'Quality Analyst' },
    { id: 3, departmentName: 'Business Development' },
    { id: 4, departmentName: 'Sales And Marketing' },
    { id: 5, departmentName: 'Tech. Support' },
    { id: 6, departmentName: 'Management' }
  ];

  constructor() {
    const employeeList = localStorage.getItem('ems_task');
    if (employeeList) {
      this.allEmployees = JSON.parse(employeeList);
    }
  }

  // Returns all the employees
  getAllEmployees(): Observable<Employee[]> {
    return of(this.allEmployees);
  }

  // Returns all the department
  getAllDepartment(): Observable<Department[]> {
    return of(this.department);
  }

  // Adds an employee to employee list
  addEmployee(employee: Employee): void {
    this.allEmployees.push(employee);
    this.storeEmployees();
  }

  // Update employee details
  updateEmployee(employee: Employee) {
    const updateEmployee = this.allEmployees.find(emp => emp.id === employee.id);
    if (updateEmployee) {
      updateEmployee.firstName = employee.firstName;
      updateEmployee.lastName = employee.lastName;
      updateEmployee.dob = employee.dob;
      updateEmployee.gender = employee.gender;
      updateEmployee.departmentName = employee.departmentName;
      this.storeEmployees();
    }
  }

  // Delete an employee from employee list
  deleteEmployee(id: string) {
    this.allEmployees = this.allEmployees.filter(employee => employee.id !== id);
    this.storeEmployees();
    return of(this.allEmployees);
  }

  // Returns an employee with passed employee id from employee list
  getEmployee(id: string): Observable<Employee> {
    const employee = this.allEmployees.find(item => item.id === id);
    return of(employee);
  }

  storeEmployees() {
    localStorage.setItem('ems_task', JSON.stringify(this.allEmployees));
  }
}
