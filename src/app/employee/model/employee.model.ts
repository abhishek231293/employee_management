export class Employee {
  public id: string;
  public firstName: string;
  public lastName: string;
  public dob: string;
  public gender: string;
  public departmentName: string;

  constructor(  id: string,
                firstName: string,
                lastName: string,
                dob: string,
                gender: string,
                departmentName: string ) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.dob = dob;
    this.gender = gender;
    this.departmentName = departmentName;
  }
}
