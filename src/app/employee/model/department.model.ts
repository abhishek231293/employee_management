export class Department {
  public id: number;
  public departmentName: string;

  constructor(  id: number,
                departmentName: string ) {
    this.id = id;
    this.departmentName = departmentName;
  }
}
