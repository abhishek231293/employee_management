import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AddEmployeeComponent} from './add-employee/add-employee.component';
import {ListEmployeeComponent} from './list-employee/list-employee.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'list'
  },
  {
    path: 'list',
    component: ListEmployeeComponent
  },
  {
    path: 'add',
    component: AddEmployeeComponent,
    data : {
      request_for : 'add',
      heading : 'Add New Employee'
    }
  },
  {
    path: 'view/:id',
    component: AddEmployeeComponent,
    data : {
      request_for : 'view',
      heading : 'Employee Profile'
   }
  },
  {
    path: 'edit/:id',
    component: AddEmployeeComponent,
    data : {
      request_for : 'edit',
      heading : 'Update Employee Detail'
    }
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeeRoutingModule { }
